#!/bin/sh
# set execution permissiobn to all!

# ping -c 1 $ip | grep "0% packet loss"
myip=$(ifconfig | grep 192.168 | awk '{print $2}' | cut -f2 -d':')
echo Local ip $myip

if [ -z "~/hunterhunt/trusted.txt" ]
  then
  echo "Trusted MACs files could not be found"
  read
  abort
  else
  echo "Trusted MACs files found"
fi

while true
do

for host in $(seq 30 50)
do

  ip="192.168.1.$host"
  echo -n "Checking IP ... $ip"
  if [ "$ip" != "$myip" ]
  then
  	  alive=$(ping -c 1 $ip | grep "100% packet loss")

	  if [ -z "$alive" -a "$ip" != "$myip" ]
	  then
		echo -n ' ' is alive

		mac_address=$(arp -v $ip | grep 'ether' | tr -s ' ' | cut -f3 -d ' ')
		echo -n ' ' MAC is $mac_address
		is_trusted=$(cat ~/hunterhunt/trusted.txt | grep -i $mac_address)
		if [ -n "$is_trusted" ]
		  then
		  echo -n ' ' trusted '->' $(echo $is_trusted | cut -d'#' -f2)
		else
		  echo -n ' ' INTRUSSION!!
		  echo
		  echo
		  vlc alarm.mp3 &
		  read
		fi
	  else
		echo -n ' ' is down
	  fi
  else
  	echo -n ' ' is local, bypassing
  fi
  echo

done
  sleep 5m
done
