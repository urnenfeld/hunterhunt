# README #

The initial purpose of this tool was to track if any unwanted user was connection to my home wireless network. The initial version was something fast without caring much about configurability.

### What is this repository for? ###


* The tool could be expanded to other shells / Window managers
* The tool misses an easy way to configure timeouts and actions
* The Network detection is mainly hardcoded.
* Action when something is detected is also hardcoded
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Place files under ~/hunter/
* Call the script ~/hunter/createHunterfile.sh
* Edit and rename trusted.example.txt to trusted.txt to add your known hosts.
* You could not add them and add them as soon as new devices are detected
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact