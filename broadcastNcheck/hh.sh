#!/bin/sh

CONF_FILE=lanW.conf

function parse_arp
{

IFS=$'\n'
for entries in $(cat /proc/net/arp | grep -v type | grep -v 00:00:00:00:00:00)
do
	mac=$(echo $entries | awk {'print $4'})
	ip=$(echo $entries | awk {'print $1'})

	is_trusted=$(cat $CONF_FILE | grep -i $mac)

	if [ -n "$is_trusted" ]
	then
	  comment=$(echo $is_trusted | cut -d'#' -f2)
	  info=$(echo $is_trusted | cut -d'#' -f1)
	  ipExpected=$(echo $info | awk {'print $2'})

	  if [ -n "$ipExpected" ]
	  then
		  if [ "$ip" != "$ipExpected" ]
		  then  # Trusted with unexpected IP 
			echo $(date +"%y/%m/%d %H:%M:%S"): WARNING! MAC'('$comment')': $mac is trusted, IP: $ip is not expected!
		  else  # Trusted with IP specified 
			echo $(date +"%y/%m/%d %H:%M:%S"): MAC'('$comment')': $mac is trusted, IP: $ip
		  fi	
	  else	  # no IP specified Trusted
	          echo $(date +"%y/%m/%d %H:%M:%S"): MAC'('$comment')': $mac is trusted, IP: $ip	
	  fi

	else
	  # NOT Trusted
          echo $(date +"%y/%m/%d %H:%M:%S"): WARNING! MAC: $mac IP: $ip UNTRUSTED!
	fi
done

}

function check_for_command 
{
	echo Checking for $1 ...
	$2 > /dev/null
	if [ $? == 0 ]; then # check the value
		echo $1 available
		return 0
	else
		echo $1 not available
		return 1
	fi
}

# 10.0.0.0/24
# 192.168.1.0/24

function perform_network_ping_broadcast
{
	check_for_command nmap "nmap --version"
	if [ $? == 0 ]; then # check the value
		nmap -n -sP $1
		return 0
	fi

	check_for_command fping "fping -v"
	if [ $? == 0 ]; then # check the value
		fping -g $1
		return 0
	fi

return 1
}

# Check for network parameter
# infer our network

perform_network_ping_broadcast $1
parse_arp

